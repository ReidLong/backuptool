//
//  Notification.swift
//  BackupTool
//
//  Created by Reid Long on 7/21/18.
//  Copyright © 2018 Reid Long. All rights reserved.
//

import Foundation

class NotificationTool  {

    func showNotification(title: String, message: String, sound: String = "Glass") -> Void {
        let shell = Shell()
        let command = "osascript -l JavaScript -e \"app=Application.currentApplication(); app.includeStandardAdditions = true; app.displayNotification('\(message)', {withTitle: '\(title)', soundName: '\(sound)'})\""
        print("Command: \(command)")
        let _ = shell.execute(command)
        print("Notification: \(title): \(message)")
    }
}
