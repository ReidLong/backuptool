//
//  main.swift
//  BackupTool
//
//  Created by Reid Long on 7/21/18.
//  Copyright © 2018 Reid Long. All rights reserved.
//

import Foundation

struct Shell {
    func stripWhitespace( _ inString: String ) -> String {
        return inString.trimmingCharacters (in: CharacterSet .whitespacesAndNewlines )
    }
    func execute( _ command: String , echo: Bool = true ) -> (Process, String) {
        let process = Process(); let pipe = Pipe()
        (process.standardError , process.standardOutput) = (pipe, pipe)
        process.launchPath = "/bin/sh"
        process.arguments = [ "-c" , command]
        process.launch()
        
        let handle = pipe.fileHandleForReading
        var availableData: Data
        var returnString = ""
        while true {
            availableData = handle.availableData
            if availableData.count == 0 { break }
            if let string = String(data: availableData, encoding: .utf8) {
                if echo == true { print (string, terminator: "" ) }
                returnString = returnString + string
            }
        }
        return (process, stripWhitespace(returnString))
    }
}

if CommandLine.argc != 4 {
    print("Usage: \(CommandLine.arguments[0]) LABEL SRC DEST")
    exit(1)
}

let label = "BackupTool: \(CommandLine.arguments[1])"
let sourceDirectory = CommandLine.arguments[2]
let backupDirectory = CommandLine.arguments[3]
print("Source: \(sourceDirectory) Destination: \(backupDirectory)")

func isValidPath(path: String) -> Bool {
    if !path.hasPrefix("/") {
        print("\(path) must be absolute")
        return false
    }
    
    if path.hasSuffix("/") {
        print("\(path) cannot contain trailing '/'")
        return false
    }
    
    return true
}

if !isValidPath(path: sourceDirectory) || !isValidPath(path: backupDirectory) {
    exit(2)
}

func keyPath(path: String) -> String {
    if let index = path.range(of: "/", options: .backwards) {
        return "...\(path[index.lowerBound...])"
    } else {
        return path
    }
}

let notification = NotificationTool()
let fileManager = FileManager.default

if !fileManager.fileExists(atPath: sourceDirectory) {
    notification.showNotification(title: label, message: "Source missing \(keyPath(path: sourceDirectory))", sound: "Basso")
    exit(3)
}

if !fileManager.fileExists(atPath: backupDirectory) {
    notification.showNotification(title: label, message: "Backup destination missing \(keyPath(path: backupDirectory))", sound: "Basso")
    exit(4)
}

let graveyard = "\(backupDirectory)/__GRAVEYARD"

let command = "rsync -ahzEb --progress --backup-dir=\"\(graveyard)\" --exclude=__GRAVEYARD --delete --stats \"\(sourceDirectory)/\" \"\(backupDirectory)\""
print("Executing: \(command)")

let (process, output) = Shell().execute(command, echo: true)


if process.terminationStatus != 0 {
    notification.showNotification(title: label, message: "Backup failed: \(process.terminationReason)", sound: "Basso")
} else {
    notification.showNotification(title: label, message: "Backup completed successfully")
}
